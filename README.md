# Bash Automated Testing System

Docker image to run Bash Automated Testing System with additional testing tools.

[BATS documentation](https://github.com/bats-core/bats-core)

## Usage

### Docker Run

```bash
docker run --rm -it -v $(pwd):/code registry.gitlab.com/jeff_cook/bats:latest
```

### GitLab CI Include

With GitLab CI you can include a pipeline file.
Make sure you have added the `post_build` stage to your pipeline.

```yaml
include:
  - project: jeff_cook/bats
    ref: 1.0.0
    file: /.gitlab-ci/.gitlab-ci.yml
```

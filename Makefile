
IMAGE = jeff_cook/bats

build:
	docker build -t ${IMAGE} .

test: build
	docker run --rm -v $(shell pwd):/code --entrypoint /bin/pwd ${IMAGE}
	docker run --rm -v $(shell pwd):/code --entrypoint /bin/ls ${IMAGE} -lR
	# docker run --rm -v $(shell pwd):/code --entrypoint /usr/bin/find ${IMAGE}
	docker run --rm -v $(shell pwd):/code ${IMAGE} --version
	docker run --rm -v $(shell pwd):/code --entrypoint curl ${IMAGE} --version
	docker run --rm -v $(shell pwd):/code --entrypoint nmap ${IMAGE} --version
	# docker run --rm -v $(shell pwd):/code --entrypoint /usr/sbin/bats ${IMAGE} -r .
	docker run --rm -v $(shell pwd):/code ${IMAGE}

push_major:
	bump2version major
	git push
	git push --tags

push_minor:
	bump2version minor
	git push
	git push --tags

push_patch:
	bump2version patch
	git push
	git push --tags

#!/usr/bin/env bats

@test "Ensure curl version 7" {
  run curl --version
  [[ "$output" == *"curl 7."* ]]
  [[ "$output" == *" LibreSSL/2."* ]]
}

@test "Ensure curl LibreSSL version 2" {
  run curl --version
  [[ "$output" == *"curl 7."* ]]
  [[ "$output" == *" LibreSSL/2."* ]]
}

@test "Ensure curl protocols" {
  run curl --version
  [[ "$output" == *" imap "* ]]
  [[ "$output" == *" imaps "* ]]
  [[ "$output" == *" smtp "* ]]
  [[ "$output" == *" smtps "* ]]
}

#!/usr/bin/env bats

@test "Ensure nmap version 7" {
  run nmap --version
  [[ "$output" == *"Nmap version 7."* ]]
}

@test "Ensure nmap openssl version 2" {
  run nmap --version
  [[ "$output" == *" openssl-2."* ]]
}

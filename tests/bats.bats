#!/usr/bin/env bats

@test "Ensure bats version 1.1.0" {
  run bats --version
  [[ "$output" == "Bats 1.1.0" ]]
}

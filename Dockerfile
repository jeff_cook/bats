# FROM bash:5.0.17
FROM bats/bats

RUN mkdir -p /code

RUN apk add \
    # bats \
    curl \
    nmap \
    nmap-scripts

WORKDIR /code

ENTRYPOINT ["/usr/sbin/bats"]
CMD ["-r", "."]
